const path = require("path")
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin")

module.exports = env => {
  const plugins = [
    new MiniCssExtractPlugin({
      filename: "assets/css/style.css",
      chunkFilename: "[name].css"
    }),
    new HtmlWebpackPlugin({
      title: 'Platzi-badges',
      template: path.resolve(__dirname, 'src/index.html')
    })
  ];

  return {
    entry: {
      app: ['@babel/polyfill', path.resolve(__dirname, "src/index.js")]
    },
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: 'js/[name].js',
      publicPath: 'http://localhost:9000/',
      chunkFilename: 'js/[id].[chunkhash].js'
    },
    devServer: {
      port: 9000,
      hot: true,
      historyApiFallback: true,
      open: true
    },
    mode: "development",
    devtool: "eval-source-map",
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          exclude: /(node_modules)/,
          use: {
            loader: "babel-loader",
            options: {
              presets: ["@babel/preset-env", "@babel/preset-react"]
            }
          }
        },
        {
          test: /\.css$/,
          use: ["style-loader", "css-loader"]
        },
        {
          test: /\.scss$/,
          use: ["style-loader", "css-loader", "sass-loader"]
        },
        {
          test: /\.(jpg|png|gif|svg)$/,
          use: {
            loader: "url-loader",
            options: {
              limit: 1000000,
              fallback: "file-loader",
              name: "images/[name].[hash].[ext]"
            }
          }
        },
        {
          test: /\.(eot|svg|ttf|woff|woff2)$/,
          loader: "file-loader",
          options: {
            name: "[name].[ext]"
          }
        }
      ]
    },
    plugins
  };
};
