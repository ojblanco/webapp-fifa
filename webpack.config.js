const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const FontminPlugin = require('fontmin-webpack')
const TersetJSPlugin = require('terser-webpack-plugin')
const AddAssetHtmlPlugin = require("add-asset-html-webpack-plugin")
const path = require('path')

module.exports = {
  output: {
    filename: 'app.bundle.js',
    publicPath: '/'
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html'
    }),
    new MiniCssExtractPlugin({
      filename: 'assets/css/style.css',
      chunkFilename: '[name].css'
    }),
    new FontminPlugin({
      autodetect: true,
      glyphs: []
    }),
    new webpack.DllReferencePlugin({
      manifest: require('./modules.manifest.json')
    }),
    new AddAssetHtmlPlugin({
      filepath: path.resolve(__dirname, "dist", "js", "*.dll.js"),
      outputPath: "js",
      publicPath: "js"
    })
  ],
  mode: 'production',
  optimization: {
    minimizer: [
      new TersetJSPlugin()
    ]
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env', '@babel/preset-react']
          }
        }
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2|otf)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'assets/fonts/[name].[ext]',
              limit: 1000,
              mimetype: 'application/font-woff'
            }
          }
        ]
      },
      {
        test: /\.(jpg|png|gif|svg)$/,
        use: {
          loader: 'url-loader',
          options: {
            limit: 1000,
            fallback: 'file-loader',
            name: 'assets/images/[name].[hash].[ext]'
          }
        }
      }
    ]
  }
}
