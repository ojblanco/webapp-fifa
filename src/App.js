import React, { Suspense } from 'react'

import { Router } from '@reach/router'

import { GlobalStyle } from './assets/styles/global'
import { Teams } from './pages/Teams'
import { Reports } from './pages/Reports'
import { AddTeam } from './pages/AddTeam'
import { AddPlayer } from './pages/AddPlayer'
import { AddTechnical } from './pages/AddTechnical'

export const App = () => {
    return (
        <Suspense>
            <GlobalStyle />
            <Router>
                <Teams path="/" />
                <Teams path="/teams" />
                <AddTeam path="/teams/add" />
                <AddPlayer path="/add/player/:id" />
                <AddTechnical path="/add/tech/:id" />
                <Reports path="/reports" />
            </Router>
        </Suspense>

    )
}
