import React from 'react'
import { Layout, ButtonLink } from '../widgets/'
import { IoIosArrowBack } from 'react-icons/io'
import { color } from '../assets/styles/variables'
import { Tech } from '../modules/technical/bloc/tech'

export const AddTechnical = ({ id }) => {
    return (
        <Layout
            title="Equipos"
        >
            <ButtonLink to="/teams" width="86px">
                <IoIosArrowBack size="20px" color={color.yellow} />
                Atrás
            </ButtonLink>
            <Tech id={id} />
        </Layout>
    )
}