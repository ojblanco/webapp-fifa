import React from 'react'
import { Layout } from '../widgets/'
import { Team } from '../modules/teams/bloc'

export const Teams = () => {
    return (
        <Layout
            title="Equipos"
        >
            <Team />
        </Layout>
    )
}
