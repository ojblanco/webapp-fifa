import React from 'react'
import { Layout, ButtonLink } from '../widgets/'
import { IoIosArrowBack } from 'react-icons/io'
import { color } from '../assets/styles/variables'
import { Player } from '../modules/players/bloc/player'

export const AddPlayer = ({ id }) => {
    return (
        <Layout
            title="Agregar Jugador"
        >
            <ButtonLink to="/teams" width="86px">
                <IoIosArrowBack size="20px" color={color.yellow} />
                Atrás
            </ButtonLink>
            <Player id={id} />
        </Layout>
    )
}