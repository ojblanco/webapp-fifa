import React from 'react'
import { Layout } from '../widgets/'
import { Report } from '../modules/reports/bloc'

export const Reports = () => {
    return (
        <Layout
            title="Reportes"
        >
            <Report />
        </Layout>
    )
}