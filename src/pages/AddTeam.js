import React from 'react'
import { Layout, ButtonLink } from '../widgets/'
import { IoIosArrowBack } from 'react-icons/io'
import { color } from '../assets/styles/variables'
import {AddTeam as Add} from '../modules/teams/bloc/add'

export const AddTeam = () => {
    return (
        <Layout
            title="Agregar Equipo"
        >
            <ButtonLink to="/teams" width="86px">
                <IoIosArrowBack size="20px" color={color.yellow} />
                Atrás
            </ButtonLink>
            <Add />
        </Layout>
    )
}