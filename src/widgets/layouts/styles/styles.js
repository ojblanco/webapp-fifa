import styled from 'styled-components'
import { color, media } from '../../../assets/styles/variables'
import { Link as LinkRouter } from '@reach/router'

const NAV_HEIGHT = '80px';
const ASIDE_WIDTH = '240px';

export const Main = styled.main`
    min-width: 100%;
    min-heigth: 100%;
    position: relative;
`

export const Aside = styled.aside`
    background-color: ${color.blue};
    position: fixed;
    top: 0;
    left: 0;
    min-height: 100%;
    width: ${ASIDE_WIDTH};

    @media(max-width: ${media.tablet}) {
        display:none;
    }
`

export const AsideMobile = styled.aside`
    background-color: ${color.blue};
    position: fixed;
    top: 0;
    left: 0;
    min-height: 100vh;
    width: ${ASIDE_WIDTH};

    @media(min-width: ${media.sDesktop}) {
        display:none;
    }
`

export const CloseButton = styled.button`
    position: absolute;
    right: 5px;
    top: 5px;
    background-color: ${color.yellow};
    height: 28px;
    width: 28px;
    border-radius: 50%;
    display: flex;
    justify-content: center;
    align-items: center;
    @media(min-width: ${media.sDesktop}) {
        display: none;
    }
`

export const ContainerLogo = styled.div`
    width: 100%;
    height: auto;
    margin-top: 20px;
    padding: 10px 0 10px 30px;
`

export const List = styled.ul`
    width: 100%;
    height: auto;
    margin-top: 20px;
`

export const Item = styled.li`
    width: 100%;
    height: 52px;
    padding: 10px 0 10px 30px;
    position: relative;
    &:after {
        background-color: rgba(255, 255, 255, 0.17);       
        content: '';
        position: absolute;
        bottom: 0;
        width: 20px;
        height: 3px;
        left: 30px;
    }
`

export const Link = styled(LinkRouter)`
    align-items: center;
    color: rgba(255, 255, 255, 0.6);
    display: inline-flex;
    height: 100%;
    text-decoration: none;
    width: 100%;
    font-size: 14px;
    transition: all ease 0.65s;

    &[aria-current] {
        color: ${color.lighten};
        font-size: 16px;
    }
`

export const Nav = styled.nav`
    width: 100%;
    height: ${NAV_HEIGHT};
    position: static;
    right: 0;
    top: 0;
    border-bottom: 0.6px solid ${color.grayLighten};
    display: flex;
    align-items: center;
`

export const ContainerButton = styled.div`
    margin-left: 10px;
    padding: 10px
    @media(min-width: ${media.sDesktop}) {
        display: none;
    }
`

export const MenuButton = styled.button`
    border: 0.5px solid ${color.grayLighten};
    border-radius: 8px;
    padding: 0 4px;
`

export const Section = styled.section`
    width: calc(100% - ${ASIDE_WIDTH});
    min-height: 100%;
    float: right;
    padding: 15px 20px;
    position: relative;
    z-index: -1;

    @media(max-width: ${media.tablet}) {
        width: 100%;
        margin-top: 10px;
    }
`

export const Title = styled.h1`
    font-size: 22px;
    font-weight: 700;
    color: ${color.textDarken}
`