import React from 'react'
import { Aside, AsideMobile, ContainerLogo, List, Item, Link, CloseButton } from '../styles/styles'
import { Logo } from './logo'
import { IoIosClose } from "react-icons/io"
import { color } from '../../../assets/styles/variables'

export const Sidenav = ({ media = 'isDesktop', handleClose }) => {
    const AdiseComponent = media === 'isDesktop' ? Aside : AsideMobile
    return (
        <AdiseComponent>
            <ContainerLogo>
                <Logo width="100px" />
                <CloseButton onClick={handleClose}>
                    <IoIosClose size='28px' color={color.lighten} />
                </CloseButton>
            </ContainerLogo>
            <List>
                <Item>
                    <Link to="/teams">Equipos</Link>
                </Item>
                <Item>
                    <Link to="/reports">Reportes</Link>
                </Item>
            </List>
        </AdiseComponent>
    )
}