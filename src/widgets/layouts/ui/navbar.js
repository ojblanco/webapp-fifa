import React from 'react'
import { Nav, MenuButton, ContainerButton } from '../styles/styles'
import { IoIosMenu } from "react-icons/io"
import { color } from '../../../assets/styles/variables';

export const Navbar = ({ handleOpen }) => {
    return (
        <Nav>
            <ContainerButton>
                <MenuButton onClick={handleOpen}><IoIosMenu size='28px' color={color.textDarken} /></MenuButton>
            </ContainerButton>
        </Nav>
    )
}