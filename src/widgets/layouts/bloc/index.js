import React, { useState } from 'react'
import { animated } from 'react-spring/renderprops'
import { Main, Section, Title } from '../styles/styles'
import { Sidenav } from '../ui/sidenav'
import { Navbar } from '../ui/navbar'
import { Sidebar } from '../ui/Sidebar'

export const Layout = ({ title = '', children }) => {

    const [open, setOpen] = useState(false)

    const state = open === undefined || !open ? 'close' : 'open'

    return (
        <Main>
            <Sidenav />
            <Sidebar
                native
                state={state}
            >
                {({ x }) => (
                    <animated.div
                        className="sidebar"
                        style={{
                            transform: x.interpolate(x => `translate3d(${x}%,0,0)`),
                        }}>
                        <Sidenav media="isMobile" handleClose={e => setOpen(false)} />
                    </animated.div>
                )}
            </Sidebar>
            <Navbar handleOpen={e => setOpen(!open)} />
            <Section>
                <Title>{title}</Title>
                {children}
            </Section>
        </Main>
    )
}