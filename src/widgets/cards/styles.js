import styled from 'styled-components'
import { media } from '../../assets/styles/variables'

export const Card = styled.div`
    width: 100%;
    height: auto;
    padding: 20px;
    border-radius: 20px;
    box-shadow: 0 2px 2px 0 rgba(0,0,0,0.12), 0 2px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.12);
    margin: 5px;
    position: relative;
    overflow: hidden;
    ${props => props.flex && "flex: 1 1 " + props.flex}
    @media(max-width: ${media.tablet}) {
        flex: 1 1 100%;
    }
`