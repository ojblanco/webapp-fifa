import styled from 'styled-components'
import { color } from '../../../assets/styles/variables'

export const Input = styled.input`
  border: 1px solid ${color.grayLighten};
  border-radius: 3px;
  font-family: 'Montserrat';
  margin-bottom: 8px;
  padding: 8px 4px;
  display: block;
  width: 100%;
  &[disabled] {
    opacity: .3;
  }
`

export const Select = styled.select`
  border: 1px solid ${color.grayLighten};
  border-radius: 3px;
  font-family: 'Montserrat';
  margin-bottom: 8px;
  padding: 8px 4px;
  display: block;
  width: 100%;
`

export const Label = styled.label`
    font-family: 'Montserrat';
    font-weight: 600;
    color: ${color.textLighten}
    font-size: 12px;
    margin: 16px 0 8px 0;
`