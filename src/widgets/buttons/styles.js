import styled from 'styled-components'
import { color } from '../../assets/styles/variables'
import { Link as LinkRouter } from '@reach/router'

export const ButtonLink = styled(LinkRouter)`
    display: flex;
    align-items: center;
    justify-content: space-between;
    color: ${color.blue};
    height: 34px;
    text-decoration: none;
    ${props => props.width ? "width: " + props.width : "width: auto"}
    font-size: 14px;
    padding: 5px 10px;
    border: 1px solid ${color.blue}
    border-radius: 4px;
    font-weight: 500;
    box-shadow: 0 2px 2px 0 rgba(0,0,0,0.12), 0 2px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.12);
    margin: 8px 0;
`
export const Button = styled.button`
    align-items: center;
    background-color: ${color.blue};
    color: ${color.lighten};
    height: 32px;
    padding: 8px 32px;
    justify-content: center;
    border-radius: 8px;
    border: none;
    cursor:pointer;
    box-shadow: 0 2px 2px 0 rgba(0,0,0,0.12), 0 2px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.12);
`