import React, { useState, useContext } from 'react'
import { Card, Label, Input, Button } from '../../../widgets'
import { useInputValue } from '../../../hooks/useInputValue'
import { ContainerCenter } from '../../../assets/styles/general'
import { generateID } from '../../../core/helpers'
import { Context } from '../../../Context'

export const Player = ({ id }) => {

    const { setNewPlayer } = useContext(Context)

    const avatar = useInputValue('')
    const firstName = useInputValue('')
    const lastName = useInputValue('')
    const born = useInputValue('')
    const position = useInputValue('')
    const number = useInputValue('')
    const holder = useInputValue('')

    const [disabled, setDisabled] = useState(false)

    const handleSubmit = (e) => {
        if (!avatar.value || !firstName.value || !lastName.value || !born.value || !position.value || !number.value || holder.value) return alert('Disculpa, debes completar el formulario.')
        setDisabled(true)
        const data = {
            avatar: avatar.value,
            firstName: firstName.value,
            lastName: lastName.value,
            born: born.value,
            position: position.value,
            number: number.value,
            holder: holder.value
        }
        setNewPlayer({ ...data, id: generateID(), team: parseInt(id) })
        setTimeout(() => {
            alert('Jugador agregado con exito.')
            window.history.back()
        }, 500);
    }

    return (
        <Card>
            <Label>Foto del jugador</Label>
            <Input type="file" {...avatar} />
            <Label>Nombre</Label>
            <Input type="text" {...firstName} />
            <Label>Apellido</Label>
            <Input type="text" {...lastName} />
            <Label>Fecha de nacimiento</Label>
            <Input type="date" {...born} />
            <Label>Posición</Label>
            <Input type="text" {...position} />
            <Label>Número de camiseta</Label>
            <Input type="number" min={0} {...number} />
            <Label>¿Es titular?</Label>
            <input type="radio" name="titular" value="true" {...holder} /><span>Si</span>
            <input type="radio" name="titular" value="false" {...holder} /><span>No</span>
            <ContainerCenter>
                <Button onClick={handleSubmit} disabled={disabled}>Guardar</Button>
            </ContainerCenter>
        </Card>
    )
}