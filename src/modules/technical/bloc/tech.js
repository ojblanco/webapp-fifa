import React, { useState, useContext } from 'react'
import { Card, Label, Input, Button } from '../../../widgets'
import { useInputValue } from '../../../hooks/useInputValue'
import { ContainerCenter } from '../../../assets/styles/general'
import { generateID } from '../../../core/helpers'
import { Context } from '../../../Context'
import { Select } from '../../../widgets/forms/styles/styles'

export const Tech = ({ id }) => {

    const { setNewTech } = useContext(Context)

    const firstName = useInputValue('')
    const lastName = useInputValue('')    
    const nationality = useInputValue('')
    const born = useInputValue('')
    const rol = useInputValue('')

    const [disabled, setDisabled] = useState(false)

    const handleSubmit = (e) => {
        if (!firstName.value || !lastName.value || !born.value || !nationality.value || !rol.value) return alert('Disculpa, debes completar el formulario.')
        setDisabled(true)
        const data = {
            firstName: firstName.value,
            lastName: lastName.value,
            born: born.value,
            nationality: nationality.value,
            rol: rol.value,
            id: generateID(),
            team: parseInt(id)
        }
        setNewTech(data)
        setTimeout(() => {
            alert('Técnico agregado con exito.')
            window.history.back()
        }, 500);
    }

    return (
        <Card>
            <Label>Nombre</Label>
            <Input type="text" {...firstName} />
            <Label>Apellido</Label>
            <Input type="text" {...lastName} />
            <Label>Fecha de nacimiento</Label>
            <Input type="date" {...born} />
            <Label>Nacionalidad</Label>
            <Input type="text" {...nationality} />
            <Label>Rol</Label>
            <Select {...rol}>
                <option value="">Seleccione</option>
                <option value="tecnico">Tecnico</option>
                <option value="asistente">Asistente</option>
                <option value="medico">Médico</option>
                <option value="preparador">Preparador</option>
            </Select>
            <ContainerCenter>
                <Button onClick={handleSubmit} disabled={disabled}>Guardar</Button>
            </ContainerCenter>
        </Card>
    )
}