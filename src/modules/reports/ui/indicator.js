import React from 'react'
import { Card } from '../../../widgets/cards/styles'
import { Item, H2, Subtitle } from '../styles/styles'

export const Indicator = ({ flex, title, text }) => (
    <Card flex={flex}>
        <Item>
            <H2>{title}</H2>
            <Subtitle>{text}</Subtitle>
        </Item>
    </Card>
)