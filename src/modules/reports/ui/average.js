import React from 'react'
import { Card } from '../../../widgets/cards/styles'
import { Subtitle, H2, Column } from '../styles/styles'

export const Average = ({ flex, subtitle, avg }) => {
    return (
        <Card flex={flex}>
            <Column>
                <Subtitle>{subtitle}</Subtitle>
                <H2>{avg}</H2>
            </Column>
        </Card>
    )
}