import React from 'react'
import { Card } from '../../../widgets/cards/styles'
import { Row, TableMain, TD, TH } from '../../../assets/styles/table'
import { AvatarMin } from '../styles/styles'

export const Table = ({ data }) => (
    <Card>
        <h2>Técnicos de nacionalidad distinta a la de su equipo.</h2>
        <TableMain>
            <thead>
                <Row>
                    <TH></TH>
                    <TH>Nombre</TH>
                    <TH>Nacionalidad</TH>
                    <TH>Selección</TH>
                </Row>
            </thead>
            <tbody>
                {
                    data.map((item, i) => (
                        <Row key={i}>
                            <TD><AvatarMin src={item.avatar} /></TD>
                            <TD>{item.name}</TD>                            
                            <TD>{item.nacionalidad}</TD>
                            <TD>{item.seleccion}</TD>
                        </Row>
                    ))
                }
            </tbody>
        </TableMain>
    </Card>
)