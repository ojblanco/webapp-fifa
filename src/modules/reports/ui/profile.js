import React from 'react'
import { Card } from '../../../widgets/cards/styles'
import { ContainerImage, Avatar, Item, Subtitle, Name } from '../styles/styles'

export const Profile = ({ flex, avatar, name, title, age }) => {
    return (
        <Card
            flex={flex}
        >
            <Item>
                <ContainerImage>
                    <Avatar src={avatar} />
                </ContainerImage>
                <Name>{name}</Name>
                <Subtitle>{title}</Subtitle>
                <h2>{age} años</h2>
            </Item>
        </Card>
    )
}