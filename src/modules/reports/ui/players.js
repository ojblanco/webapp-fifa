import React from 'react'
import { Card } from '../../../widgets/cards/styles'
import flag from '../../../assets/images/flag.png'
import { ContainerImage, Avatar, Subtitle, Name, H2, Column, Item } from '../styles/styles'

export const Players = ({ team, players }) => {
    return (
        <Card>
            <Column>
                <ContainerImage>
                    <Avatar src={flag} />
                </ContainerImage>
                <Item>
                    <Subtitle>Selección que registró más jugadores: </Subtitle>
                    <Name>{team}</Name>
                </Item>
                <H2>{players}</H2>
            </Column>
        </Card>
    )
}