import React from 'react'
import { Container } from '../styles/styles'
import { Indicator } from '../ui/indicator'
import { Profile } from '../ui/profile'
import { Average } from '../ui/average'
import { Players } from '../ui/players'
import { Table } from '../ui/table'


const data = [
    { title: '34', text: 'Equipos registrados' },
    { title: '713', text: 'Jugadores participantes' },
    { title: '87', text: 'Jugadores suplentes' }
]

const people = [
    { avatar: 'https://pbs.twimg.com/profile_images/1113204197091049472/ahcSOTJI.jpg', name: 'Matthijs De Ligt', title: 'Jugador más joven', age: 19 },
    { avatar: 'https://m.media-amazon.com/images/M/MV5BNzY1YTI3NzMtNGIwMi00OWZkLThjMjgtZmI1Y2EyYTI3ODJkXkEyXkFqcGdeQXVyMjQwMzc1MzI@._V1_UY256_CR5,0,172,256_AL_.jpg', name: 'Kazuyoshi Miura', title: 'Jugador más longevo', age: 51 },
    { avatar: 'https://pbs.twimg.com/profile_images/1081279702612897792/vm2OT_gQ.jpg', name: 'Oscar Washington Tabárez', title: 'Técnico más longevo', age: 71 },
]

const averages = [
    { subtitle: 'Promedio de suplentes por equipo: ', avg: 19 },
    { subtitle: 'Promedio de jugadores por equipo: ', avg: 41 },
    { subtitle: 'Edad promedio de los jugadores: ', avg: 24 }
]

const DT = [
    { name: 'Jhon Jeffrey', seleccion: 'Brasil', nacionalidad: 'Estadounidense', avatar: 'https://images-na.ssl-images-amazon.com/images/M/MV5BMjA5MjExMzY0M15BMl5BanBnXkFtZTgwNTMxOTg4MTI@._V1_UY256_CR10,0,172,256_AL_.jpg' },
    { name: 'Fernando Ortiz Montero', seleccion: 'Francia', nacionalidad: 'Chileno', avatar: 'https://randomuser.me/api/portraits/men/75.jpg' },
    { name: 'Mario Muñoz Gallardo', seleccion: 'Argentina', nacionalidad: 'Español', avatar: 'https://i.imgur.com/CN7EmnR.jpg' },
    { name: 'Neil Ruppert', seleccion: 'Costa Rica', nacionalidad: 'Holandés', avatar: 'https://i.imgur.com/a1rGmoj.jpg' },
    { name: 'Devan Brant', seleccion: 'Italia', nacionalidad: 'Australiano', avatar: 'https://images-na.ssl-images-amazon.com/images/M/MV5BMTA4NDg5MTc4NzNeQTJeQWpwZ15BbWU3MDEyMDIwNjE@._V1_UY256_CR2,0,172,256_AL_.jpg' },
    { name: 'Jaime Puig Carrasco', seleccion: 'Inglaterra', nacionalidad: 'Colombiano', avatar: 'https://i.imgur.com/gBj4KHl.jpg' },
]

export const Report = () => {
    return (
        <Container>
            {
                data.map((item, i) => (
                    <Indicator key={i} {...item} flex="32%"/>
                ))
            }
            {
                people.map((item, i) => (
                    <Profile key={i} {...item} flex="32%"/>
                ))
            }
            {
                averages.map((item, i) => (
                    <Average key={i} {...item} flex="32%"/>
                ))
            }
            <Players
                team="Alemania"
                players="42"
            />
            <Table data={DT} />
        </Container>
    )
}