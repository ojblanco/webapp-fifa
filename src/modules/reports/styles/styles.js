import styled from 'styled-components'
import { color, media } from '../../../assets/styles/variables'

export const Container = styled.div`
    width: 100%;
    display: flex;
    flex-wrap: wrap;
`
export const Item = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
`

export const Column = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;

    @media(max-width: ${media.sDesktop}) {
        flex-direction: column;
    }
`

export const H2 = styled.h2`
    width: 84px;
    height: 84px;
    font-size: 40px;
    border-radius: 50%;
    background-color: ${color.blue};
    color: ${color.yellow}
    margin-bottom: 16px;
    position:relative;
    display: flex;
    justify-content: center;
    align-items: center;

    &:after{
        content: '';
        position: absolute;
        width: 120%;
        height: 1px;
        bottom: -8px;
        left: 50%;
        transform: translateX(-50%);
        background-color: ${color.grayLighten}
    }
`

export const Subtitle = styled.p`
    font-weight: 600;
    font-size: 14px;
    margin-bottom: 8px;
    text-align: center;
    flex-basis: 60%;
`

export const ContainerImage = styled.div`
    width: 110px;
    height: 110px;
    position: relative;
    overflow: hidden;
    border-radius: 50%;
    margin-bottom: 8px;
`

export const Avatar = styled.img`
    position: absolute;
    object-fit: cover;
    width: 100%;
    height: 100%;
`

export const AvatarMin = styled.img`
    background-size: cover;
    width: 30px;
    height: 30px;
    border-radius: 50%;
`

export const Name = styled.h2`
    color: ${color.blue}
    position: relative;
    padding: 8px 0;
    text-align: center;
    height: 74px;
    font-size: 24px;
    &:before{
        content: '';
        position: absolute;
        width: 80%;
        height: 1px;
        top: 0;
        left: 50%;
        transform: translateX(-50%);
        background-color: ${color.grayLighten}
    }

    @media(max-width: ${media.sDesktop}) {
        height: auto;
    }
`