import React, { Fragment, useContext } from 'react'
import { Table } from '../ui/table'
import { ButtonLink } from '../../../widgets'
import { IoIosAdd } from 'react-icons/io'
import { color } from '../../../assets/styles/variables'
import { Context } from '../../../Context'

export const Team = () => {
    const { teams } = useContext(Context)
    return (
        <Fragment>
            <ButtonLink to="/teams/add" width="156px">
                <IoIosAdd color={color.yellow} size="20px" /> Agregar equipo
            </ButtonLink>
            <Table data={teams} />
        </Fragment>
    )
}