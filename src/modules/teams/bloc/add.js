import React, { useContext, useState } from 'react'
import { Form } from '../ui/form'
import { Context } from '../../../Context'
import { generateID } from '../../../core/helpers'

export const AddTeam = () => {

    const { setNewTeam } = useContext(Context)
    const [disabled, setDisabled] = useState(false)

    const handleOnSubmit = data => {
        if (!data.name || !data.flag || !data.shield) return alert('Disculpa, debes completar el formulario.')
        setDisabled(true)
        setNewTeam({ ...data, id: generateID() })
        setTimeout(() => {
            alert('Equipo agregado con exito.')
            window.history.back()
        }, 500);
    }

    return (
        <Form onSubmit={handleOnSubmit} disabled={disabled} />
    )
}