import React from 'react'
import { Row, TableMain, TD, TH } from '../../../assets/styles/table'
import { Card } from '../../../widgets/cards/styles'
import { ButtonLink } from '../../../widgets'
import { IoIosAdd } from "react-icons/io";
import { color } from '../../../assets/styles/variables';

export const Table = ({ data = [] }) => {
    return (
        <Card>
            <TableMain>
                <thead>
                    <Row>
                        <TH>Nombre del Equipo</TH>
                        <TH>Bandera</TH>
                        <TH>Escudo</TH>
                        <TH></TH>
                    </Row>
                </thead>
                <tbody>
                    {
                        data.map((item, i) => (
                            <Row key={i}>
                                <TD>{item.name}</TD>
                                <TD><img src={item.flag} /></TD>
                                <TD><img src={item.shield} /></TD>
                                <TD>
                                    <ButtonLink to={`/add/player/${item.id}`} width="160px">
                                        <IoIosAdd color={color.yellow} size="20px" /> Agregar jugador
                                    </ButtonLink>
                                    <ButtonLink to={`/add/tech/${item.id}`} width="160px">
                                        <IoIosAdd color={color.yellow} size="20px" /> Agregar técnico
                                    </ButtonLink>
                                </TD>
                            </Row>
                        ))
                    }
                </tbody>
            </TableMain>
        </Card>
    )
}