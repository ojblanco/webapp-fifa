import React from 'react'
import { Card, Input, Label, Button } from '../../../widgets'
import { useInputValue } from '../../../hooks/useInputValue'
import { ContainerCenter } from '../../../assets/styles/general'

export const Form = ({ onSubmit, disabled }) => {

    const name = useInputValue('')
    const flag = useInputValue('')
    const shield = useInputValue('')

    const handleSubmit = (e) => {
        e.preventDefault()
        onSubmit({ name: name.value, flag: flag.value, shield: shield.value })
    }

    return (
        <Card>
            <Label>Nombre del equipo</Label>
            <Input placeholder="Nombre del equipo" type="text" {...name} />
            <Label>Bandera del equipo</Label>
            <Input placeholder="Bandera del equipo" type="file" {...flag} />
            <Label>Escudo del equipo</Label>
            <Input placeholder="Escudo del equipo" type="file" {...shield} />
            <ContainerCenter>
                <Button onClick={handleSubmit} disabled={disabled}>Guardar</Button>
            </ContainerCenter>
        </Card>
    )
}