import React, { createContext, useState } from 'react'
export const Context = createContext()

const Provider = ({ children }) => {
    const [teams, setTeams] = useState(() => {
        try {
            const item = window.localStorage.getItem('teams')                       
            return item !== null ? JSON.parse(item) : []
        } catch (error) {            
            return []
        }
        return []
    })

    const [players, setPlayers] = useState(() => {
        try {
            const item = window.localStorage.getItem('players')                       
            return item !== null ? JSON.parse(item) : []
        } catch (error) {            
            return []
        }
        return []
    })

    const [techs, setTechs] = useState(() => {
        try {
            const item = window.localStorage.getItem('techs')                       
            return item !== null ? JSON.parse(item) : []
        } catch (error) {            
            return []
        }
        return []
    })

    const value = {
        teams,
        players,
        techs,
        setNewTeam: _team => {
            setTeams([...teams, _team])
            window.localStorage.setItem('teams', JSON.stringify([...teams, _team]))
        },
        setNewPlayer: _player => {
            setPlayers([...players, _player])
            window.localStorage.setItem('players', JSON.stringify([...players, _player]))
        },
        setNewTech: _tech => {
            setTechs([...techs, _tech])
            window.localStorage.setItem('techs', JSON.stringify([...techs, _tech]))
        }
    }

    return (
        <Context.Provider value={value}>
            {children}
        </Context.Provider>
    )
}

export default {
    Provider,
    Consumer: Context.Consumer
}