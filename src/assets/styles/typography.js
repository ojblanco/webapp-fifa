import { css } from 'styled-components'
import { fontWeight } from './variables'

import MontserratRegular from '../fonts/Montserrat/Montserrat-Regular.ttf'
import MontserratItalic from '../fonts/Montserrat/Montserrat-Italic.ttf'
import MontserratBold from '../fonts/Montserrat/Montserrat-Bold.ttf'
import MontserratBlack from '../fonts/Montserrat/Montserrat-Black.ttf'
import MontserratBlackItalic from '../fonts/Montserrat/Montserrat-BlackItalic.ttf'

export const fontFaces = css`
  @font-face {
    font-family: 'Montserrat';
    src: url(${MontserratRegular}) format('truetype');
    font-weight: ${fontWeight.regular};
    font-style: normal;
    font-display: block;
  }

  @font-face {
    font-family: 'Montserrat';
    src: url(${MontserratItalic}) format('truetype');
    font-weight: ${fontWeight.regular};
    font-style: italic;
    font-display: block;
  }

  @font-face {
    font-family: 'Montserrat';
    src: url(${MontserratBold}) format('truetype');
    font-weight: ${fontWeight.bold};
    font-style: normal;
    font-display: block;
  }

  @font-face {
    font-family: 'Montserrat';
    src: url(${MontserratBlack}) format('truetype');
    font-weight: ${fontWeight.black};
    font-style: normal;
    font-display: block;
  }

  @font-face {
    font-family: 'Montserrat';
    src: url(${MontserratBlackItalic}) format('truetype');
    font-weight: ${fontWeight.black};
    font-style: italic;
    font-display: block;
  }
`
