import { createGlobalStyle } from 'styled-components'

import { fontFaces } from './typography'
import { color } from './variables'

export const GlobalStyle = createGlobalStyle`
  ${fontFaces}

  html {
    box-sizing: border-box;
    font-family: 'Montserrat', system-ui;
    color: ${color.textLighten}
  }

  * {
    margin: 0;
    padding: 0;
  }

  *, *:before, *:after {
    box-sizing: inherit;
  }

  ul { list-style: none; }

  button { background: transparent; border: 0; outline: 0; font-family: "Montserrat", system-ui;}

  a, a:visited, a:link, a:active {
    text-decoration: none;
  }

  a, button {
    -webkit-tap-highlight-color: transparent;
  }

  #app {
    position: relative;
  }
`
