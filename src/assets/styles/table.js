import styled from 'styled-components'
import { color } from './variables'

export const TableMain = styled.table`
    width: 100%;
    margin-top: 15px;
`

export const Row = styled.tr`
    height: 30px;
    overflow: hidden;
    border-bottom: 1px solid ${color.grayLighten}
`

export const TH = styled.th`
    width: auto;
    height: 30px;
    text-align: left;
    padding: 2px 0;
`

export const TD = styled.td`
    width: auto;
    height: 30px;
    border-bottom: 1px solid ${color.grayLighten}
    padding: 2px 0;
`