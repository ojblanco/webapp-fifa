export const fontWeight = {
  regular: 400,
  bold: 700,
  black: 900
}

export const color = {
  yellow: '#fc0',
  lighten: 'white',
  blueLighten: '#64FFFF',
  blue: '#01579b',
  grayLighten: '#b5b5b5cf',
  textLighten: '#535353',
  textDarken: '#131313'
}

export const media = {
  desktop: '1900px',
  laptop: '1288px',
  sDesktop: '1024px',
  tablet: '768px',
  mobile: '600px',
  sMobile: '420px'
}
