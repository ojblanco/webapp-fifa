import styled from 'styled-components'

export const ContainerCenter = styled.div`
    width: 100%;
    height: auto;
    display: flex;
    justify-content: center;
    align-items: center;
    paddign: .8rem;
`